﻿namespace _飞行棋游戏
{
    class Program
    {
        //用静态字段模拟全局变量
        static int[] Maps = new int[100];

        //声明静态数组存储玩家A和B的坐标
        static int[] PlayerPos = new int[2];

        //声明静态数组存储玩家A和B的姓名
        static string[] PlayerName = new string[2];

        //两个玩家是否执行这一轮游戏的标记
        static bool[] Flags = new bool[2]; //bool类型默认为false

        static void Main(string[] args)
        {
            GameShow();
            #region 输入玩家姓名
            Console.WriteLine("请输入玩家A的姓名");
            PlayerName[0] = Console.ReadLine();
            while (PlayerName[0] == "")
            {
                Console.WriteLine("玩家A的姓名不能为空，请重新输入");
                PlayerName[0] = Console.ReadLine();
            }
            Console.WriteLine("请输入玩家B的姓名");
            PlayerName[1] = Console.ReadLine();
            //玩家B的判断条件就是不能为空并且不能与玩家A相同。
            while (PlayerName[1] == "" || PlayerName[0] == PlayerName[1])
            {
                if (PlayerName[1] == "")
                {
                    Console.WriteLine("玩家B的姓名不能为空，请重新输入");
                    PlayerName[1] = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("玩家B的姓名不能跟玩家A的相同，请重新输入");
                    PlayerName[1] = Console.ReadLine();
                }
            }
            #endregion
            //玩家姓名输入之后，首先应该清屏
            Console.Clear(); //清屏
            GameShow();
            Console.WriteLine("{0}的士兵用A表示", PlayerName[0]);
            Console.WriteLine("{0}的士兵用B表示", PlayerName[1]);
            InitializeTheMap();
            DrawMap();
            while (PlayerPos[0] < 99 && PlayerPos[1] < 99)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (!Flags[i])
                        PlayGame(i);
                    else
                        Flags[i] = false;

                    if (PlayerPos[i] == 99)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine("玩家{0}无耻地赢了玩家{1}", PlayerName[i], PlayerName[1 - i]);
                        break;
                    }
                }
            }
            Win();
        }

        /// <summary>
        /// 画游戏头
        /// </summary>
        public static void GameShow()
        {
            //Console.ForegroundColor控制台字体颜色
            //Console.BackgroundColor控制台背景颜色
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("************************************");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("************************************");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("************************************");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("*************飞行棋游戏*************");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("************************************");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("************************************");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("************************************");
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// 初始化地图
        /// </summary>
        public static void InitializeTheMap()
        {
            int[] luckyturn = { 6, 23, 40, 55, 69, 83 }; //幸运轮盘◎
            for (int i = 0; i < luckyturn.Length; i++)
                Maps[luckyturn[i]] = 1;

            int[] landMine = { 5, 13, 17, 33, 38, 50, 64, 80, 94 }; //地雷☆
            for (int i = 0; i < landMine.Length; i++)
                Maps[landMine[i]] = 2;

            int[] pause = { 9, 27, 60, 93 }; //暂停▲
            for (int i = 0; i < pause.Length; i++)
                Maps[pause[i]] = 3;
            int[] timeTunnel = { 20, 25, 45, 63, 72, 88, 90 }; //时空隧道卐
            for (int i = 0; i < timeTunnel.Length; i++)
                Maps[timeTunnel[i]] = 4;
        }

        /// <summary>
        /// 绘制地图
        /// </summary>
        public static void DrawMap()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("图例：幸运轮盘：◎\t地雷：☆\t暂停：▲\t时空隧道：卐");
            Console.ForegroundColor = ConsoleColor.White;

            #region 第一横行
            for (int i = 0; i < 30; i++)
            {
                Console.Write(DrawStringMap(i));
                Console.ForegroundColor = ConsoleColor.White;
            }
            Console.WriteLine();
            #endregion

            #region 第一竖行
            for (int i = 30; i < 35; i++)
            {
                for (int j = 0; j <= 28; j++)
                {
                    Console.Write("  "); //两个半角空格
                }
                Console.Write(DrawStringMap(i));
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
            }
            #endregion

            #region 第二横行
            for (int i = 64; i >= 35; i--)
            {
                Console.Write(DrawStringMap(i));
                Console.ForegroundColor = ConsoleColor.White;
            }
            Console.WriteLine();
            #endregion

            #region 第二竖行
            for (int i = 65; i <= 69; i++)
            {
                Console.WriteLine(DrawStringMap(i));
                Console.ForegroundColor = ConsoleColor.White;
            }
            #endregion

            #region 第三横行
            for (int i = 70; i <= 99; i++)
            {
                Console.Write(DrawStringMap(i));
                Console.ForegroundColor = ConsoleColor.White;
            }
            Console.WriteLine();
            #endregion
        }

        /// <summary>
        /// 判断地图中每个位置的字符串
        /// </summary>
        /// <param name="i">当前需要绘制的位置</param>
        /// <returns>返回该位置的字符串</returns>
        public static string DrawStringMap(int i)
        {
            #region 画图
            string str = "";
            //如果玩家A和玩家B的坐标相同，并且都在地图上时，画一个尖括号
            if (PlayerPos[0] == PlayerPos[1] && PlayerPos[0] == i)
            {
                str = "<>";
            }
            else if (PlayerPos[0] == i)
            {
                //全角：shift + 空格
                str = "Ａ";
            }
            else if (PlayerPos[1] == i)
            {
                str = "Ｂ";
            }
            else
            {
                switch (Maps[i])
                {
                    case 0:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        str = "□";
                        break;
                    case 1:
                        Console.ForegroundColor = ConsoleColor.Green;
                        str = "◎";
                        break;
                    case 2:
                        Console.ForegroundColor = ConsoleColor.Red;
                        str = "☆";
                        break;
                    case 3:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        str = "▲";
                        break;
                    case 4:
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        str = "卐";
                        break;
                }
            }
            return str;
            #endregion
        }

        /// <summary>
        /// 玩家玩游戏的步骤
        /// </summary>
        /// <param name="playerNumber"></param>
        public static void PlayGame(int playerNumber)
        {
            Random r = new Random();
            int rNumber = r.Next(1, 7);
            Console.WriteLine("{0}按任意键开始掷色子", PlayerName[playerNumber]);
            Console.ReadKey(true); //控制台不会输入键盘输入的值
            Console.WriteLine("{0}掷出了{1}", PlayerName[playerNumber], rNumber);
            PlayerPos[playerNumber] += rNumber;
            ChangePos();
            Console.WriteLine("{0}按任意键开始行动", PlayerName[playerNumber]);
            Console.ReadKey(true);
            Console.WriteLine("{0}行动完了", PlayerName[playerNumber]);

            //玩家A踩到玩家B
            if (PlayerPos[playerNumber] == PlayerPos[1 - playerNumber]) //数学技巧
            {
                Console.WriteLine("玩家{0}踩到了玩家{1}，玩家{2}后退6格", PlayerName[playerNumber], PlayerName[1 - playerNumber], PlayerName[1 - playerNumber]);
                PlayerPos[1 - playerNumber] -= 6;
                ChangePos();
                Console.ReadKey(true);
            }
            else //踩到了关卡
            {
                //玩家的坐标
                switch (Maps[PlayerPos[playerNumber]])
                {
                    case 0:
                        Console.WriteLine("玩家{0}踩到了方块，什么都没有发生", PlayerName[playerNumber]);
                        Console.ReadKey(true);
                        break;
                    case 1:
                        Console.WriteLine("玩家{0}踩到了幸运轮盘，请选择 1--交换位置 2--轰炸对方", PlayerName[playerNumber]);
                        string input = Console.ReadLine();
                        while (true)
                        {
                            if (input == "1")
                            {
                                Console.WriteLine("玩家{0}选择跟玩家{1}交换位置", PlayerName[playerNumber], PlayerName[1 - playerNumber]); ;
                                Console.ReadKey(true);
                                int temp = PlayerPos[playerNumber];
                                PlayerPos[playerNumber] = PlayerPos[1 - playerNumber];
                                PlayerPos[1 - playerNumber] = temp;
                                Console.WriteLine("交换完成！！！按任意键继续游戏！！！");
                                Console.ReadKey(true);
                                break;
                            }
                            else if (input == "2")
                            {
                                Console.WriteLine("玩家{0}选择轰炸玩家{1}，玩家{2}后退6格", PlayerName[playerNumber], PlayerName[1 - playerNumber], PlayerName[1 - playerNumber]);
                                PlayerPos[1 - playerNumber] -= 6;
                                ChangePos();
                                Console.ReadKey(true);
                                break;
                            }
                            else
                            {
                                Console.WriteLine("只能输入1或者2 1--交换位置 2--轰炸对方");
                                input = Console.ReadLine();
                            }
                        }
                        break;
                    case 2:
                        Console.WriteLine("玩家{0}踩到了地雷，后退6格", PlayerName[playerNumber]);
                        Console.ReadKey(true);
                        PlayerPos[playerNumber] -= 6;
                        ChangePos();
                        break;
                    case 3:
                        Console.WriteLine("玩家{0}踩到了暂停，暂停一回合", PlayerName[playerNumber]);
                        Flags[playerNumber] = true;
                        Console.ReadKey(true);
                        break;
                    case 4:
                        Console.WriteLine("玩家{0}踩到了时空隧道，前进10格", PlayerName[playerNumber]);
                        PlayerPos[playerNumber] += 10;
                        ChangePos();
                        Console.ReadKey(true);
                        break;
                }//switch
            }//else
            Console.Clear();
            DrawMap();
        }

        /// <summary>
        /// 当玩家坐标发生改变的时候调用
        /// </summary>
        public static void ChangePos()
        {
            for (int i = 0; i < 2; i++)
            {
                if (PlayerPos[i] < 0)
                    PlayerPos[i] = 0;
                if (PlayerPos[i] > 99)
                    PlayerPos[i] = 99;
            }
        }

        /// <summary>
        /// 胜利
        /// </summary>
        public static void Win()
        {
            Console.ForegroundColor = ConsoleColor.Red;
        }
    }
}