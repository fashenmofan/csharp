﻿namespace _49判断闰年
{
    class Program
    {
        static void Main(string[] args)
        {
            //举例：写一个方法，判断一个年份是否是闰年.
            Console.WriteLine("请输入年份");
            int year = Convert.ToInt32(Console.ReadLine());
            bool flag = IsRun(year);
            Console.WriteLine(flag);
        }

        /// <summary>
        /// 判断给定的年份是否是闰年
        /// </summary>
        /// <param name="year">要判断的年份</param>
        /// <returns>是否是闰年</returns>
        public static bool IsRun(int year)
        {
            bool b = (year % 400 == 0) || (year % 4 == 0 && year % 100 != 0);
            return b;
        }
    }
}