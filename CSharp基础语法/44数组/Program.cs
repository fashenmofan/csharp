﻿namespace _44数组
{
    class Program
    {
        static void Main(string[] args)
        {
            //常用的声明方式1：数组类型[] 数组名 = new 数组类型[数组长度];
            int[] nums = new int[10];
            string[] str = new string[10];
            bool[] bools = new bool[10];

            //需要记住的声明方式2
            int[] nums2 = { 1, 2, 3, 4, 5, 6 };

            //不常用的声明方式3
            int[] nums3 = new int[3] { 1, 2, 3 };

            //不常用的声明方式4
            int[] num4 = new int[] { 1, 2, 3, 4, 5, 6 }; 

            //nums[0] = 1;
            //nums[1] = 2;
            //nums[2] = 3;
            //nums[3] = 4;
            //nums[4] = 5;
            //nums[5] = 6;
            //nums[6] = 7;
            //nums[7] = 8;
            //nums[8] = 9;
            //nums[9] = 10;

            for (int i = 0; i < 10; i++)
            {
                nums[i] = i + 1;
            }
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}