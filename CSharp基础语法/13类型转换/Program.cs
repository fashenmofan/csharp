﻿namespace _13类型转换
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 10;
            //int ---> double
            //目标类型 = 源类型
            double d = number; //自动类型转换 隐式类型转换

            double d1 = 303.6;
            //double ---> int
            //目标类型 = (目标类型)原类型
            int n = (int)d1;//强制类型转换，显示类型转换
            Console.WriteLine(n);

            int n1 = 10;
            int n2 = 3;
            double d2 = n1 / n2;
            Console.WriteLine(d2); //3
            double d3 = n1 * 1.0 / n2;
            Console.WriteLine(d3); //3.3333333333333335
            Console.WriteLine("{0:0.00}", d3); //保留两位小数


            string s = "123";
            //string ---> double或者int
            double s_d = Convert.ToDouble(s);
            int s_i = Convert.ToInt32(s);
            Console.WriteLine("转成double为{0}，转成int为{1}", s_d, s_i);

            #region 练习1:让用户输入姓名 语文 数学 英语 三门课的成绩，然后给用户显示:XX，你的总成绩为XX分，平均成绩为XX分。
            Console.WriteLine("请输入你的姓名");
            string name = Console.ReadLine();
            Console.WriteLine("请输入你的语文成绩");
            string strChinese = Console.ReadLine();
            Console.WriteLine("请输入你的数学成绩");
            string strMath = Console.ReadLine();
            Console.WriteLine("请输入你的英语成绩");
            string strEnglish = Console.ReadLine();

            int intChinese = Convert.ToInt32(strChinese);
            int intMath = Convert.ToInt32(strMath);
            int intEnglish = Convert.ToInt32(strEnglish);
            Console.WriteLine("{0}，你的总成绩为{1}分，平均成绩为{2}分", name, intChinese + intMath + intEnglish, (intChinese + intMath + intEnglish) / 3);

            double doubleChinese = Convert.ToDouble(strChinese);
            double doubleMath = Convert.ToDouble(strMath);
            double doubleEnglish = Convert.ToDouble(strEnglish);
            Console.WriteLine("{0}，你的总成绩为{1}分，平均成绩为{2:0.000}分", name, doubleChinese + doubleMath + doubleEnglish, (doubleChinese + doubleMath + doubleEnglish) / 3);
            #endregion


            #region 练习2:提示用户输入一个数字 接收 并且向控制台打印用户输入的这个数字的2倍
            Console.WriteLine("请输入一个数字");
            //string strNumber = Console.ReadLine();
            double doubleNumber = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(doubleNumber * 2);
            //本题说明的就是 简写方式。
            #endregion
        }
    }
}