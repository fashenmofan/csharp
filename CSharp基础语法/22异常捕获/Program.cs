﻿namespace _22异常捕获
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 0; //声明一个变量
            bool flag = true;
            Console.WriteLine("请输入一个数字");
            try
            {
                //这行代码会出异常，所以变量需要赋初值
                number = Convert.ToInt32(Console.ReadLine()); //赋值
            }
            catch
            {
                Console.WriteLine("输入的内容不能转换成数字");
                flag = false;
            }

            //让代码满足某些条件去执行的话，使用bool类型
            if (flag)
                Console.WriteLine(number * 2);
        }
    }
}