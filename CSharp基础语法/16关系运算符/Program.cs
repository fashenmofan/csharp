﻿namespace _16关系运算符
{
    class Program
    {
        static void Main(string[] args)
        {
            //大象的重量(1500) > 老鼠的重量(1)
            bool b = 1500 > 1;
            Console.WriteLine(b);
            //兔子的寿命(3) < 乌龟的寿命(1000)
            b = 39 < 18;
            Console.WriteLine(b);
            //我的年龄(20) == 你的年龄(20) 
            b = 20 == 20;
            Console.WriteLine(b);
        }
    }
}