﻿namespace _14两道练习题
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 练习1：编程实现计算几天(如46天)是几周几天？
            int days1 = 46;
            int weeks = days1 / 7;
            int day = days1 % 7;
            Console.WriteLine("{0}天是{1}周零{2}天", days1, weeks, day);
            #endregion

            #region 练习2：编程实现107653秒几天几小时几分钟几秒？
            int seconds = 107653;
            int days2 = seconds / 86400;
            seconds %= 86400; //利用了变量的重复赋值的特征。
            int hours = seconds / 3600;
            seconds %= 3600;
            int minutes = seconds / 60;
            seconds %= 60;
            Console.WriteLine("即{0}天{1}小时{2}分钟{3}秒", days2, hours, minutes, seconds);
            #endregion
        }
    }
}