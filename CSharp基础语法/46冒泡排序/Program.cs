﻿namespace _46冒泡排序
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums = {9, 8, 7, 6, 5, 4, 3, 2, 1};

            //只能针对数组做一个升序排列
            Array.Sort(nums);
            //对数组进行反转
            Array.Reverse(nums);
            //有了这两个函数就能实现升序和降序排列

            //冒泡排序
            //for (int i = 0; i < nums.Length - 1; i ++)
            //{
            //    for (int j = 0; j < nums.Length - i - 1; j ++)
            //    {
            //        if (nums[j] > nums[j + 1])
            //        {
            //            int temp = nums[j];
            //            nums[j] = nums[j + 1];
            //            nums[j + 1] = temp;
            //        }
            //    }
            //}

            for (int i = 0; i < nums.Length; i++)
                Console.WriteLine(nums[i]);
        }
    }
}