﻿namespace _19if结构
{
    class Program
    {
        static void Main(string[] args)
        {
            //编程实现：如果跪键盘的时间大于60分钟，那么媳妇奖励我晚饭不用做了。
            Console.WriteLine("请输入你跪键盘的时间");
            int mins = Convert.ToInt32(Console.ReadLine());
            
            if (mins > 60)
            {
                Console.WriteLine("好老公，不用跪键盘了，去吃屎吧!");
            }
            //bool b = mins > 60;
            //if(b)
            //{
            //    Console.WriteLine("好老公，不用跪键盘了，去吃屎吧!");
            //}

            #region 练习1：让用户输入年龄，如果输入的年龄大于23岁（含23），则给用户显示你到了结婚的年龄了。
            Console.WriteLine("请输入你的年龄");
            int age = Convert.ToInt32(Console.ReadLine());
            if (age >= 23)
            {
                Console.WriteLine("你可以结婚啦");
            }
            #endregion

            #region 练习2：如果老苏的(Chinese music), 语文成绩大于90并且音乐成绩大于80 或者 语文成绩等于100并且音乐成绩大于70，则奖励100元。
            Console.WriteLine("请输入老苏的语文成绩");
            int chinese = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入老苏的音乐成绩");
            int music = Convert.ToInt32(Console.ReadLine());

            if ((chinese > 90 && music > 80) || (chinese == 100 && music > 70))
            {
                Console.WriteLine("奖励100元");
            }
            #endregion

            #region 练习3：让用户输入用户名和密码，如果用户名为admin，密码为mypass，则提示登录成功。
            Console.WriteLine("请输入用户名");
            string admin = Console.ReadLine();
            Console.WriteLine("请输入密码");
            string pwd = Console.ReadLine();

            if (admin == "admin" && pwd == "mypass")
            {
                Console.WriteLine("登录成功");
            }
            //本题说明核实密码时，字符串就行，不需要转成数值类型。
            #endregion
        }
    }
}