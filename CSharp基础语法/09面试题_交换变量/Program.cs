﻿namespace _09面试题_交换变量
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1 = 10;
            int n2 = 20;
            //存储空间没有发生变化。自欺欺人
            Console.WriteLine("萝卜交换后，n1的值是{0}，n2的值是{1}", n2, n1);

            //使用第三方变量进行交换
            int temp = n1;
            n1 = n2;
            n2 = temp;
            Console.WriteLine("使用第三方变量交换后，n1的值是{0}，n2的值是{1}", n1, n2);

            #region 面试题
            //请交换两个int类型的变量，要求：不使用第三方的变量
            n1 = 10;
            n2 = 20;
            n1 = n1 + n2;
            n2 = n1 - n2;
            n1 = n1 - n2;
            Console.WriteLine("不使用第三方变量交换后，n1的值是{0}，n2的值是{1}", n1, n2);
            /*或者这样
            n1 = n1 - n2;
            n2 = n1 + n2;
            n1 = n2 - n1;
             */
            #endregion
        }
    }
}