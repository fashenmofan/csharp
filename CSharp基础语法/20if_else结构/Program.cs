﻿namespace _20if_else结构
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 练习1：如果小赵的考试成绩大于90(含)分，那么爸爸奖励他100元钱，否则的话，爸爸就让小赵跪方便面。
            Console.WriteLine("请输入小赵的考试成绩");
            int score = Convert.ToInt32(Console.ReadLine());
            
            if (score >= 90)
            {
                Console.WriteLine("奖励你一百块");
            }
            else
            {
                Console.WriteLine("去跪方便面");
            }
            #endregion

            /*练习2：对学员的结业考试成绩评测
             * 成绩>=90：A
             * 90>成绩>=80：B
             * 80>成绩>=70：C
             * 70>成绩>=60：D
             * 成绩 < 60：E
             */
            #region 最正确的做法(if-else-if做法)
            Console.WriteLine("请输入学员的考试成绩");
            int ordinaryScore3 = Convert.ToInt32(Console.ReadLine());
            
            if (ordinaryScore3 >= 90)
            {
                Console.WriteLine("A");
            }
            else if (ordinaryScore3 >= 80)
            {
                Console.WriteLine("B");
            }
            else if (ordinaryScore3 >= 70)
            {
                Console.WriteLine("C");
            }
            else if (ordinaryScore3 >= 60)
            {
                Console.WriteLine("D");
            }
            else
            {
                Console.WriteLine("E");
            }
            #endregion

            #region if做法
            Console.WriteLine("请输入学员的考试成绩");
            int ordinaryScore = Convert.ToInt32(Console.ReadLine());
            if (ordinaryScore >= 90 && ordinaryScore <= 100)
            {
                Console.WriteLine("A");
            }
            if (ordinaryScore >= 80 && ordinaryScore < 90)
            {
                Console.WriteLine("B");
            }
            if (ordinaryScore >= 70 && ordinaryScore < 80)
            {
                Console.WriteLine("C");
            }
            if (ordinaryScore >= 60 && ordinaryScore < 70)
            {
                Console.WriteLine("D");
            }
            //else
            //{
            //    Console.WriteLine("E");
            //}
            //本题说明 else永远只跟离它最近的那个if配对。
            //所以也要给成下面的代码
            if (ordinaryScore < 60)
            {
                Console.WriteLine("E");
            }
            #endregion

            #region if-else做法
            Console.WriteLine("请输入学员的考试成绩");
            int ordinaryScore1 = Convert.ToInt32(Console.ReadLine());
            if (ordinaryScore1 >= 90 && ordinaryScore1 <= 100)
            {
                Console.WriteLine("A");
            }
            else
            {
                if (ordinaryScore1 >= 80)
                {
                    Console.WriteLine("B");
                }
                else
                {
                    if (ordinaryScore1 >= 70)
                    {
                        Console.WriteLine("C");
                    }
                    else
                    {
                        if (ordinaryScore1 >= 60)
                        {
                            Console.WriteLine("D");
                        }
                        else
                        {
                            Console.WriteLine("E");
                        }
                    }
                }
            }
            #endregion
        }
    }
}