﻿namespace _59方法的综合练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：提示用户输入两个数字 计算这两个数字之间所有整数的和
            //1、用户只能输入数字
            //2、计算两个数字之间和
            //3、要求第一个数字比第二个数字小 就重新输入
            Console.WriteLine("请输入第一个数字");
            string strNumberOne = Console.ReadLine();
            int numberOne = GetNumber(strNumberOne);
            Console.WriteLine("请输入第二个数字");
            string strNumberTwo = Console.ReadLine();
            int numberTwo = GetNumber(strNumberTwo);
            //两种方法，一种添加ref关键字，另一种是在方法中直接执行。
            JudgeNumber(ref numberOne, ref numberTwo);
            Console.WriteLine(GetSum(numberOne, numberTwo));
        }

        public static int GetNumber(string s)
        {
            while (true)
            {
                try
                {
                    int number = Convert.ToInt32(s);
                    return number;
                }
                catch
                {
                    Console.WriteLine("输入有误！！！请重新输入");
                    s = Console.ReadLine();
                }
            }
        }

        public static void JudgeNumber(ref int n1, ref int n2)
        {
            while (true)
            {
                if (n1 < n2)
                {
                    Console.WriteLine(GetSum(n1, n2));
                    return;
                }
                else
                {
                    Console.WriteLine("第一个数字不能够大于或者等于第二个数字，请重新输入第一个数字");
                    string s = Console.ReadLine();
                    n1 = GetNumber(s);
                    Console.WriteLine("请重新输入第二个数字");
                    s = Console.ReadLine();
                    n2 = GetNumber(s);
                }
            }
        }

        public static int GetSum(int n1, int n2)
        {
            int sum = 0;
            for (int i = n1; i <= n2; i ++)
            {
                sum += i;
            }
            return sum;
        }
    }
}