﻿namespace _17逻辑运算符
{
    class Program
    {
        static void Main(string[] args)
        {
            //让用户输入老苏的语文和数学成绩,输出以下判断是否正确,正确输出True, 错误输出False
            #region 1.老苏的语文和数学成绩都大于 90 分
            Console.WriteLine("小苏，请输入你的语文成绩");
            int chinese1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("小苏，请输入你的数学成绩");
            int math1 = Convert.ToInt32(Console.ReadLine());
            bool b1 = chinese1 > 90 && math1 > 90;
            Console.WriteLine(b1);
            #endregion

            #region 2.语文和数学有一门是大于 90 分的
            Console.WriteLine("小苏，请输入你的语文成绩");
            int chinese2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("小苏，请输入你的数学成绩");
            int math2 = Convert.ToInt32(Console.ReadLine());
            bool b2 = chinese2 > 90 || math2 > 90;
            Console.WriteLine(b2);
            #endregion
        }
    }
}