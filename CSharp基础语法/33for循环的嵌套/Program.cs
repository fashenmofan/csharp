﻿namespace _33for循环的嵌套
{
    class Program
    {
        static void Main(string[] args)
        {
            //for循环的嵌套
            for (int i = 0; i < 10; i ++)
            {
                for (int j = 0; j < 10; j ++)
                {
                    Console.WriteLine("i循环了{0}次，j循环了{1}", i, j);
                }
            }

            //乘法口诀表
            for (int i = 1; i <= 9; i ++)
            {
                for (int j = 1; j <= 9; j ++)
                {
                    Console.Write("{0}*{1}={2}\t", i, j, i * j);
                }
                Console.WriteLine();
            }

            //三角乘法口诀表
            for (int i = 1; i <= 9; i ++)
            {
                for (int j = 1; j <= i; j ++)
                {
                    Console.Write("{0}*{1}={2}\t", i, j, i * j);
                }
                Console.WriteLine();
            }    
        }
    }
}