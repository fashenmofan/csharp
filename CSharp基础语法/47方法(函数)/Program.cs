﻿namespace _47方法
{
    class Program
    {
        static void Main(string[] args)
        {
            //计算两个整数之间的最大值
            int max = Program.GetMax(1, 3); //实参
            Console.WriteLine(max);

            max = GetMax(1, 3); //实参
            Console.WriteLine(max);
        }

        /// <summary>
        /// 计算两个整数之间的最大值并返回最大值
        /// </summary>
        /// <param name="n1">第一个整数</param>
        /// <param name="n2">第二个整数</param>
        /// <returns>返回最大值</returns>
        public static int GetMax(int n1, int n2) //形参
        {
            return n1 > n2 ? n1 : n2;
        }
    }
}