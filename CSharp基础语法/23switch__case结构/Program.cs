﻿namespace _23switch_case结构
{
    class Program
    {
        static void Main(string[] args)
        {
            //李四的年终工作评定, 如果定为  A  级, 则工资涨 500 元, 如果定为  B  级,
            //则工资涨 200 元, 如果定为C级, 工资不变, 如果定为D级工资降200元,
            //如果定为E级工次降 500 元.
            //设李四的原工资为 5000 , 请用户输入李四的评级,然后显示李四来年的工资
            #region if-else-if结构
            double salary = 5000;
            bool flag = true;
            Console.WriteLine("请输入对李四的你年终评定");
            string level = Console.ReadLine();

            if (level == "A")
            {
                salary += 500;
            }
            else if (level == "B")
            {
                salary += 200;
            }
            else if (level == "C")
            {
            }
            else if (level == "D")
            {
                salary -= 200;
            }
            else if (level == "E")
            {
                salary -= 500;
            }
            else
            {
                Console.WriteLine("输入有误，程序退出");
                flag = false;
            }

            if (flag)
                Console.WriteLine("李四来年工资是{0}", salary);
            #endregion

            #region switch-case结构
            salary = 5000;
            flag = true;
            Console.WriteLine("请输入对李四多的年终评定");
            level = Console.ReadLine();
            switch (level)
            {
                case "A": salary += 500;
                    break;
                case "B": salary += 200;
                    break;
                case "C": break;
                case "D": salary -= 200;
                    break;
                case "E": salary -= 500;
                    break;
                default: Console.WriteLine("输出有误，程序退出");
                    flag = false;
                    break;
            }
            if (flag)
            {
                Console.WriteLine("李四明年的工资是{0}元", salary);
            }
            #endregion
        }
    }
}