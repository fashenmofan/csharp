﻿namespace _03变量的使用规则
{
    class Program
    {
        static void Main(string[] args)
        {
            int number; //声明或者定义了整数类型的变量
            //Console.WriteLine(number); //说明变量使用前需要赋初值
            number = 100;
            Console.WriteLine(number);
            Console.ReadKey();

            double d = 36.6;
            Console.WriteLine(d);
            Console.ReadKey(true);
        }
    }
}