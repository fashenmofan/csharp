﻿namespace _10接收用户的输入
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入你的姓名");
            //接收用户在控制台的输入
            //要求字符串进行接收，因为用户输入可能为11, 3.14, 男等等。
            string name = Console.ReadLine();

            Console.WriteLine("尊贵的{0}，您好\\(@^0^@)/", name); //"\"需要转义.
            Console.ReadKey();

            #region 练习一：问用户喜欢吃什么水果(fruits)，假如用户输入"苹果"，则显示"哈哈，这么巧，我也喜欢吃苹果！"
            Console.WriteLine("美女，你喜欢吃啥子水果呢~~~~~~");
            string fruits = Console.ReadLine();
            Console.WriteLine("这么巧呀，我也喜欢吃{0}", fruits);
            Console.ReadKey();
            #endregion

            #region 练习二：请用户输入姓名性别年龄，当用户按下某个键后在屏幕上显示"您好:XX您的年龄是XX是个X生"
            //加上你现在还不会截取字符串
            Console.WriteLine("请输入你的姓名");
            string name2 = Console.ReadLine();
            Console.WriteLine("请输入您的性别");
            string gender = Console.ReadLine();
            Console.WriteLine("请输入您的年龄");
            string age = Console.ReadLine();

            Console.WriteLine("{0}您的年龄是{1}是一个{2}生", name2, age, gender);
            Console.ReadKey();
            #endregion
        }
    }
}