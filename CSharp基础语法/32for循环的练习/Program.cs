﻿namespace _32for循环的练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：求1-100之间的所有整数和 偶数和 奇数和
            #region 答案
            //本题说明表达3不要写的太死。
            int sum = 0;
            for (int i = 1; i <= 100; i ++)
            {
                sum += i;
            }
            int odd = 0;
            for (int i = 2; i <= 100; i += 2)
            {
                odd += i;
            }
            int even = 0;
            for (int i = 1; i <= 100; i += 2)
            {
                even += i;
            }
            Console.WriteLine("整数和{0} 偶数和{1} 奇数和{2}", sum, even, odd);
            #endregion

            /*练习2：找出100-999之间的水仙花数？
             *水仙花数指的就是 这个百位数字的
             *百位的立方 + 十位的立方 + 个位的立方 == 当前这个百位数字
             */
            #region 答案
            int cnt = 0;
            for (int i = 100; i <= 999; i ++)
            {
                int bai = i / 100;
                int shi = i / 10 % 10;
                int ge = i % 10;
                if (bai * bai *bai + shi * shi * shi + ge * ge * ge == i)
                {
                    cnt++;
                    Console.WriteLine("第{0}个水仙花数{1}", cnt, i);
                }
            }
            #endregion

            //本题说明表达式2可能是一个变量。
            Console.WriteLine("请输入一个数字");
            int number = Convert.ToInt32(Console.ReadLine());
            
            for (int i = 0; i <= number; i ++)
            {
                Console.WriteLine("{0}+{1}={2}", i, number - i, number);
            }
        }
    }
}