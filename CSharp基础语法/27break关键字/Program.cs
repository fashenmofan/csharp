﻿namespace _27break关键字
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int j = 0;
            while (i <= 10)
            {
                i++;
                while (j <= 10)
                {
                    Console.WriteLine("我是里面的while循环");
                    j++;
                    break;
                }
                Console.WriteLine("我是外面的while循环");
            }
        }
    }
}