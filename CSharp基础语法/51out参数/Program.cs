﻿namespace _51out参数
{
    class Program
    {
        static void Main(string[] args)
        {
            //写一个方法 求一个数组中的最大值、最小值、总和、平均值
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            //将要返回的4个值，房子啊一个数组中返回
            //int[] res = GetMaxMinSumAvg(numbers);
            //Console.WriteLine("最大值是{0}，最小值是{1}，总和是{2}，平均值是{3}", res[0], res[1], res[2], res[3]);

            int max;
            int min;
            int sum;
            int avg;
            Test(numbers, out max, out min, out sum, out avg);
            Console.WriteLine("最大值是{0}，最小值是{1}，总和是{2}，平均值是{3}", max, min, sum, avg);
        }

        /// <summary>
        /// 计算一个数组的最大值、最小值、总和、平均值
        /// </summary>
        /// <param name="nums">要计算的数组</param>
        /// <returns>返回的数组存储最大值、最小值、总和、平均值</returns>
        public static int[] GetMaxMinSumAvg(int[] nums)
        {
            int[] res = new int[4];
            //假设 res[0] 最大值 res[1]最小值 res[2]总和 res[3]平均值
            res[0] = nums[0];
            res[1] = nums[0];
            res[2] = 0;
            for (int i = 0; i < nums.Length; i ++)
            {
                if (nums[i] > res[0]) res[0] = nums[i];
                if (nums[i] < res[0]) res[1] = nums[i];
                res[2] += nums[i];
            }
            res[3] = res[2] / nums.Length;
            return res;
        }

        /// <summary>
        /// 计算一个数组的最大值、最小值、总和、平均值
        /// </summary>
        /// <param name="nums">要计算的数组</param>
        /// <param name="max">多余返回的最大值</param>
        /// <param name="min">多余返回的最小值</param>
        /// <param name="sum">多余返回的总和</param>
        /// <param name="avg">多余返回的平均值</param>
        public static void Test(int[] nums, out int max, out int min, out int sum, out int avg)
        {
            //out参数要求在方法的内部必须为其赋值
            max = nums[0];
            min = nums[1];
            sum = 0;
            for (int i = 0; i < nums.Length; i ++)
            {
                if (nums[i] > max) max = nums[i];
                if (nums[i] < min) min = nums[i];
                sum += nums[i];
            }
            avg = sum / nums.Length;
        }
    }
}