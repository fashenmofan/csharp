﻿namespace _05赋值运算符
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 10;
            n = 20; //称为变量的重新赋值，一旦给一个变量重新赋值，那么老值就不存在了，取而代之的是新值。
            Console.WriteLine(n);
            Console.ReadKey();
        }
    }
}