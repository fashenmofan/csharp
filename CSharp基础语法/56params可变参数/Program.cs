﻿namespace _56params可变参数
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] scores = {99, 98,97};
            Test("张三", 99, 98, 77, 66, 99);

            //练习：求任意长度数组的最大值 整数类型的
            int[] nums = {1, 2, 3, 4, 5};
            int sum = GetMax(1, 3, 4, 5, 5, 6);
            Console.WriteLine("数组总和为{0}", sum);
        }

        public static int GetMax(params int[] nums)
        {
            int sum = 0;
            for (int i = 0; i < nums.Length; i ++)
            {
                sum += nums[i];
            }
            return sum;
        }

        public static void Test(string name, params int[] scores) //可变参数数组
        {
            int sum = 0;
            for (int i = 0; i < scores.Length; i ++)
            {
                sum += scores[i];
            }
            Console.WriteLine("{0}这次考试的总成绩是{1}", name, sum);
        }
    }
}