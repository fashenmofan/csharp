﻿namespace _55ref练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //使用方法来交换两个int类型的变量
            int n1 = 10;
            int n2 = 20;

            //方法一：
            //int temp = n1;
            //n1 = n2;
            //n2 = temp;

            //方法二：
            //n1 = n1 + n2;
            //n2 = n1 - n2;
            //n1 = n1 - n2;

            //方法三：
            swop(ref n1, ref n2);
            Console.WriteLine("n1为{0}，n2为{1}", n1, n2);
        }

        public static void swop(ref int n1, ref int n2)
        {
            int temp = n1;
            n1 = n2;
            n2 = temp;
        }
    }
}