﻿namespace _42结构
{
    public struct Person
    {
        public string _name; //字段
        public int _age;
        public Gender _gender;
    }

    public enum Gender
    {
        男,
        女
    }
    class Program
    {
        static void Main(string[] args)
        {
            //xx大学管理系统
            //姓名、性别、年龄、年级 //一个年级可能有5000人甚至更多。
            //string zsName = "张三";
            //int zsAge = 21;
            //char zsGender = '男';
            //int zsGrade = 3;

            Person zsPerson;
            zsPerson._name = "张三";
            zsPerson._age = 21;
            zsPerson._gender = Gender.女;

            Person lsPerson;
            lsPerson._name = "李四";
            lsPerson._age = 22;
            lsPerson._gender = Gender.男;
        }
    }
}