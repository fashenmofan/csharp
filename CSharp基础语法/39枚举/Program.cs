﻿namespace _39枚举
{
    /// <summary>
    /// 声明一个枚举变量 这个变量的类型是Gender
    /// </summary>
    public enum Gender
    {
        男,
        女
    }
    class Program
    {
        static void Main(string[] args)
        {
            //为什么会有枚举这个东西呢？
            //xx大学管理系统
            //姓名 性别 年龄 系别 年级
            //性别表示方法很多
            char gender = '男';
            string s1 = "male";
            string ss1 = "爷们";
            //枚举可以规范我们的开发
            
            //变量类型 变量名 = 值;
            int n = 10;
            //枚举的使用
            Gender genderTest = Gender.女;
        }
    }
}