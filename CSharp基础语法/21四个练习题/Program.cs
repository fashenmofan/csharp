﻿namespace _21四个练习题
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：比较3个数字的大小，不考虑相当
            #region 答案
            Console.WriteLine("请输入第一个数字");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入第二个数字");
            int num2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入第三个数字");
            int num3 = Convert.ToInt32(Console.ReadLine());

            //三种情况 应该使用 if-else-if来做
            if (num1 > num2 && num1 > num3)
            {
                Console.WriteLine("第一个数字{0}大", num1);
            }
            else if (num2 > num1 && num2 > num3)
            {
                Console.WriteLine("第二个数字{0}大", num2);
            }
            else if (num3 > num1 && num3 > num2)
            {
                Console.WriteLine("第三个数字{0}大", num3);
            }
            #endregion

            //练习2：提示用户输入密码，如果密码是“88888”则提示正确，否则要求再输入一次，
            //如果密码是“88888”则提示正确，否则提示错误，程序结束。
            //(如果我的密码里有英文还是转换吗，密码：abc1)
            #region 答案
            Console.WriteLine("请输入密码");
            string pwd1 = Console.ReadLine();
            if (pwd1 == "888888")
            {
                Console.WriteLine("登录成功");
            }
            else
            {
                Console.WriteLine("密码错误，请重新输入");
                pwd1 = Console.ReadLine();
                if (pwd1 == "888888")
                {
                    Console.WriteLine("输了两遍，终于正确了");
                }
                else
                {
                    Console.WriteLine("两边都不对，程序结束");
                }
            }
            #endregion

            //练习2: 提示用户输入用户名, 然后再提示输入密码,
            //如果用户 名是 "admin"前且密码是 "88888", 则提示正确,
            //否则, 如果用户名不是admin则不提示用户用户名不存在,如果用户名是admin则提示密码错误。
            #region 答案
            Console.WriteLine("请输入用户名");
            string name = Console.ReadLine();
            Console.WriteLine("请输入密码");
            string pwd2 = Console.ReadLine();

            if (name == "admin" && pwd2 == "888888")
            {
                Console.WriteLine("登录成功");
            }
            else if (name == "admin")
            {
                Console.WriteLine("密码输入错误，程序退出");
            }
            else
            {
                Console.WriteLine("用户名不存在");
            }
            #endregion

            //练习3: 提示用户输入年龄, 如果大于等于18, 则告知用户可以査看,
            //如果小于10岁, 则告知不允许査看,
            //如果大于等于10岁并且小于18, 则提示用户是否继续査看 (yes、no) ,
            //如果输入的是yes则提示用户请査看, 否则提示"退出, 你放弃查看"。
            #region
            Console.WriteLine("请输入你的年龄");
            int age = Convert.ToInt32(Console.ReadLine());

            if (age >= 18)
            {
                Console.WriteLine("看吧，早晚你都要知道");
            }
            else if (age < 10)
            {
                Console.WriteLine("滚蛋，回家吃奶去");
            }
            else
            {
                Console.WriteLine("确定要看吗？yes/no");
                string input = Console.ReadLine();
                if (input == "yes")
                {
                    Console.WriteLine("看吧，早熟的孩子，后果自负哦");
                }
                else
                {
                    Console.WriteLine("乖孩子，回家吃奶去吧");
                }
            }
            #endregion
        }
    }
}