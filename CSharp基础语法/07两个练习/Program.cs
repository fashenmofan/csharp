﻿namespace _08两个练习
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 练习一
            //有个叫卡卡西的人在旅店登记的时候前台让他填一张表, 
            //这张表的里的内容要存到电脑上,
            //有姓名、年龄、邮箱、家庭住址,工资.
            //之后把这些信息显示出来
            string name = "卡卡西";
            int age = 30;
            string email = "kakaxi@qq.com";
            string address = "火影村";
            decimal salary = 5000m;
            Console.WriteLine("我叫" + name + ",我住在" + address + ",我今年" + age + "岁了," + "我的邮箱是" + email + "。");
            Console.WriteLine("我叫{0}，我住在{1}，我今年{2}岁了，我的邮箱是{3}", name, address, age, email);
            Console.ReadKey();
            #endregion

            #region 练习二
            int age1 = 18;
            age1 = 81;
            Console.WriteLine("原来你都" + age1 + "岁了呀");
            Console.ReadKey();
            #endregion
        }
    }
}