﻿namespace _26while循环
{
    class Program
    {
        static void Main(string[] args)
        {
            //向控制台打印100遍 下次考试我一定要细心
            //循环体: Console. WriteLine("下次考试我一定要细心");
            //循环条件:打印的次数小于 100 遍
            //需要定义一个循环变量用来记录循环的次数 没循环一次，循环变量都应该自身加1
            #region 答案
            int i = 0;
            while (i < 100)
            {
                Console.WriteLine("下次考试我一定要细心");
                i++; //每循环一次，都要自身加1.
            }
            #endregion

            //求1-100之间所有整数的和
            //循环体：累加的过程
            //循环条件：i <= 100
            #region 答案
            i = 1;
            int sum = 0; //声明一个变量用来存储总和
            while (i <= 100)
            {
                sum += i;
                i++;
            }
            Console.WriteLine("总和为", sum);
            #endregion

            //要求用户输入用户名和密码 用户名只要不是admin 密码不是888888
            //循环体:提示用户输入用户名 我们接收 密码 接收 判断是否登录成功
            //循环条件:用户名或密码错误
            #region 该答案存在不足之处，因为一般网站都会有容错率，输入3次以上会有冷却时间
            string userName = "";
            string userPwd = "";
            while (userName != "admin" || userPwd != "888888")
            {
                Console.WriteLine("请输入用户名");
                userName = Console.ReadLine();
                Console.WriteLine("请输入密码");
                userPwd = Console.ReadLine();
            }
            Console.WriteLine("登录成功");
            #endregion
        }
    }
}