﻿namespace _31for循环的正序和倒序输出
{
    class Program
    {
        static void Main(string[] args)
        {
            //请打印 1-10
            for (int i = 1; i <= 10; i ++)
            {
                Console.WriteLine(i);
            }
            //打印10-1
            for (int i = 10; i >= 1; i --)
            {
                Console.WriteLine(i);
            }
        }
    }
}