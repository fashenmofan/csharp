﻿namespace _45数组的5个练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：从一个整数数组中取出最大的整数,最小整数,总和,平均值
            //声明一个int类型的数组，并且随意的赋初值
            #region 答案
            int[] nums = new int[10];
            Random r = new Random();
            for (int i = 0; i < 10; i++)
                nums[i] = r.Next(1, 10);
            int sum = 0;
            //比较最大值最小值时，最好将数组中的一个值赋给max和min，这个值作为参照物
            int max = nums[0];
            int min = nums[0];
            //int所能表示的最大值和最小值
            //int max = int.MaxValue;
            //int min = int.MinValue;
            for (int i = 0; i < 10; i++)
            {
                sum += nums[i];
                if (max < nums[i]) max = nums[i];
                if (min > nums[i]) min = nums[i];
            }
            Console.WriteLine("这个整数数组\n最大值为{0}\n最小值为{1}\n总和为{2}\n平均值为{3}", max, min, sum, sum / 10);
            #endregion

            //练习2：数组里面都是人的名字,分割成: 例如: 老杨 | 老苏 | 老邹..."
            //       (老杨, 老苏, 老邹, 老虎, 老牛, 老蒋, 老王, 老马)
            #region 答案
            string[] names = { "老杨", "老苏", "老邹", "老虎", "老牛", "老蒋", "老王", "老马" };
            string name = "";
            for (int i = 0; i < names.Length; i++)
            {
                name += names[i];
                if (i != names.Length - 1)
                    name += "|";
            }
            Console.WriteLine(name);
            #endregion

            //练习3：将一个整数数组的每一个元素进行如下的处理:
            //如果元素是正数则将这个位置的元素的值加1,
            //如果元素是负数则将这个位置的元素的值减1,
            //如果元素是0,则不变。
            #region 答案
            nums = new int[10];
            for (int i = 0; i < 10; i++)
                nums[i] = r.Next(-10, 10);
            for (int i = 0; i < nums.Length; i++)
                if (nums[i] < 0) nums[i]--;
                else if (nums[i] > 0) nums[i]++;
            for (int i = 0; i < nums.Length; i++)
                Console.WriteLine(nums[i]);
            #endregion


            //练习4：将一个字符串数组的元素的顺序进行反转。
            //{“我", “是","好人"}{“好人","是","我"}。
            //第i个和第length - i - 1个进行交换。
            #region 答案
            string[] s = { "我", "是", "好人" };
            for (int i = 0; i < s.Length / 2; i ++)
            {
                string temp = s[i];
                s[i] = s[s.Length - i - 1];
                s[s.Length - i - 1] = temp;
            }

            for (int i = 0; i < s.Length; i++)
                Console.Write(s[i]);
            #endregion
        }
    }
}