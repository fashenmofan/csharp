﻿namespace _61方法的练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：接受输入后判断其等级并显示出来。
            //判断依据如下：等级={优（90 100分）；良（80 89分）}
            Console.WriteLine("请输入考试成绩");
            int score = Convert.ToInt32(Console.ReadLine());
            string level = GetLevel(score);
            Console.WriteLine(level);

            //练习2：请将字符串数组{"中国", "美国", "巴西", "澳大利亚", "加拿大"}中的内容反转
            string[] names = { "中国", "美国", "巴西", "澳大利亚", "加拿大" };
            //names.Reverse();
            Reversal(ref names);
            for (int i = 0; i < names.Length; i++)
                Console.WriteLine(names[i]);

            //练习3：写一个方法，计算圆的面积和周长，面积是PI*R*R，周长是2*PI*r
            Console.WriteLine("请输入圆的半径");
            double r = Convert.ToDouble(Console.ReadLine());
            double perimeter;
            double area;
            GetPerimeterArea(r, out perimeter, out area);
            Console.WriteLine("圆的面积为{0}周长为{1}", perimeter, area);

            //练习4：计算任意多个数之间的最大值（提示：params）
            int sum = GetSum(1, 2, 4, 6, 7);
            Console.WriteLine(sum);

            //练习5：请通过冒泡排序对整数数组{1, 3, 5, 7, 90, 2, 4, 6, 8, 10}实现升序排序
            int[] nums = { 1, 3, 5, 7, 90, 2, 4, 6, 8, 10 };
            BubbleSort(nums); //传递是地址
            for (int i = 0; i < nums.Length; i++)
                Console.WriteLine(nums[i]);

            //练习6：将字符串数组输出为|分割的形式（用方法来实现此功能）
            string[] strs = { "梅西", "卡卡", "郑大世" };
            string str = ProcessString(strs);
            Console.WriteLine(str);
        }

        public static string ProcessString(string[] names)
        {
            string str = null;
            for (int i = 0; i < names.Length - 1; i ++)
            {
                str += names[i] + "|";
            }
            return str + names[names.Length - 1];
        }

        public static void BubbleSort(int[] nums)
        {
            for (int i = 0; i < nums.Length - 1; i ++)
            {
                for (int j = 0; j < nums.Length - 1 - i; j ++)
                {
                    if (nums[j] > nums[j + 1])
                    {
                        int temp = nums[j];
                        nums[j] = nums[j + 1];
                        nums[j + 1] = temp;
                    }
                }
            }
        }

        public static int GetSum(params int[] nums)
        {
            int sum = 0;
            for (int i = 0; i < nums.Length; i ++)
            {
                sum += nums[i];
            }
            return sum;
        }

        public static void GetPerimeterArea(double r, out double perimeter, out double area)
        {
            perimeter = 2 * 3.14 * r;
            area = 3.14 * r * r;
        }

        public static void Reversal(ref string[] names)
        {
            for (int i = 0; i < names.Length / 2; i++)
            {
                string temp = names[i];
                names[i] = names[names.Length - i - 1];
                names[names.Length - i - 1] = temp;
            }
        }

        public static string GetLevel(int score)
        {
            string level = "";
            switch (score / 10)
            {
                case 10:
                case 9: level = "优"; break;
                case 8: level = "良"; break;
                case 7: level = "中"; break;
                case 6: level = "差"; break;
                default: level = "不及格"; break;
            }
            return level;
        }
    }
}