﻿namespace _54ref参数
{
    class Program
    {
        static void Main(string[] args)
        {
            double salary = 5000;
            Bonus(ref salary);
            Console.WriteLine(salary);
        }

        public static void Bonus(ref double s)
        {
            s += 500;
        }

        public static void Fine(ref double s)
        {
            s -= 500;
        }
    }
}