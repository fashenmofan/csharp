﻿namespace _29do_while循环
{
    class Program
    {
        static void Main(string[] args)
        {
            //明天小兰就要登台演出了, 老师说再把明天的演出的歌曲唱一遍,
            //如果满意, 小兰就可以回家了. 否则就需要再练习一遍, 直到老师满意为止.  (y / n) I 
            #region 答案以及分析
            //循环体:小兰唱了一遍 问老师 满意么? 老师回答
            //循环条件:老师不满意

            //while版本不适用
            //Console.WriteLine("老师我唱的你满意么？");
            //string answer = Console.ReadLine();
            //while (answer == "no")
            //{
            //    Console.WriteLine("老师，我再唱一遍，你满意么？");
            //    answer = Console.ReadLine();
            //}

            //do-while
            string answer;
            do
            {
                Console.WriteLine("老师，我再唱一遍，你满意么？");
                answer = Console.ReadLine();
            } while (answer == "no");
            Console.WriteLine("OK，放学回家~~~");
            #endregion

            //练习1：要求用户输入用户名和密码，只要不是admin、888888就一直提示用户名或密码错误,请重新输入
            #region 答案
            string name = "";
            string pwd = "";
            do
            {
                Console.WriteLine("请输入用户名");
                name = Console.ReadLine();
                Console.WriteLine("请输入密码");
                pwd = Console.ReadLine();
            } while (name != "admin" || pwd != "888888");
            Console.WriteLine("登录成功");
            #endregion

            //练习2：不断要求用户输入学生姓名，输入q结束
            #region 答案
            string stuName = "";
            do
            {
                Console.WriteLine("请输入学生姓名，输入q结束");
                stuName = Console.ReadLine();
            } while (stuName != "q");
            #endregion

            //练习3：不断要求用户输入一个数字，然后打印这个数字的二倍，当用户输入q的时候程序退出。
            #region 答案以及分析
            //循环体：提示用户输入一个数字 接收 转换 打印2倍
            //循环条件：输入的不能是q
            string input = "";
            while (input != "q")
            {
                Console.WriteLine("请输入一个数字，我们讲打印这个数字的二倍");
                //不能直接转换成int类型 因为用户可能输入字符类型
                input = Console.ReadLine();
                if (input != "q")
                {
                    try
                    {
                        int number = Convert.ToInt32(input);
                        Console.WriteLine("你输入的数字的2倍是{0}", number);
                    }
                    catch
                    {
                        Console.WriteLine("输入的字符串不能够转换成数字，请重新输入");
                    }
                }
                else
                {
                    Console.WriteLine("输入的是q，程序退出");
                }
            }
            #endregion

            //练习4：不断要求用户输入一个数字（假定用户输入的都是正整数），当用户输入end的时候显示刚才输入的数字中的最大值
            #region 答案以及分析
            //循环体：提示用户输入一个数字 接收 转换成int类型 不停的比较大小
            //循环条件：输入的不是end
            input = "";
            int max = 0;
            while (input != "end")
            {
                Console.WriteLine("请输入一个数字，输入end我们显示你输入的最大值");
                input = Console.ReadLine();
                if (input != "end")
                {
                    try
                    {
                        int number = Convert.ToInt32(input);
                        if (number > max)
                        {
                            max = number;
                        }
                    }
                    catch
                    {
                        Console.WriteLine("输入的字符串不能够转换成数字，请重新输入");
                    }
                }
                else
                {
                    Console.WriteLine("您刚才输的数字中的最大值是{0}", max);
                }

            }
            #endregion
        }
    }
}