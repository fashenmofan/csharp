﻿namespace _30for循环
{
    class Program
    {
        static void Main(string[] args)
        {
            //向控制台打印10遍 欢迎来到这里学习
            #region while循环
            int i = 0; //定义循环的次数
            while (i < 10)
            {
                Console.WriteLine("{0}欢迎来到这里学习", i + 1);
                i++;
            }
            #endregion

            #region for循环
            for (i = 0; i < 10; i ++)
            {
                Console.WriteLine("欢迎来到这里学习");
            }
            #endregion
        }
    }
}