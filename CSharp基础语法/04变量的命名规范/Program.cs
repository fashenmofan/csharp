﻿namespace _05变量的命名规范
{
    class Program
    {
        static void Main(string[] args)
        {
            int number_1 = 10;
            int number_2 = 20;

            //满足规范之前，一定要有意义，这里的a和b除了自己其他人都不知道。
            int a = 10;
            int b = 10;

            //在C#中，大小写是敏感的
            int Number = 10;
            int number = 10;

            //变量不允许重复的声明或者定义
            //int number;
            //int number = 10;
        }
    }
}