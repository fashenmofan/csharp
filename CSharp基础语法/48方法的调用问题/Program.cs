﻿namespace _48方法的调用问题
{
    class Program
    {
        //字段 属于类的字段
        public static int _number = 10;

        static void Main(string[] args)
        {
            int a = 3;
            Test(a);
            Console.WriteLine(a);
            Console.WriteLine(_number);
        }

        public static int Test(int a)
        {
            a = a + 5;
            return a;
        }

        public static void TestTwo()
        {
            Console.WriteLine(_number);
        }
    }
}