﻿namespace _25判断闰年
{
    class Program
    {
        static void Main(string[] args)
        {
            //请用户输入年份，再输入月份，输入该月的天数。（结合之间如何判断闰年来做）
            Console.WriteLine("请输入一个年份");
            try
            {
                int year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入一个月份");
                try
                {
                    int month = Convert.ToInt32(Console.ReadLine());
                    if (month >= 1 && month <= 12)
                    {
                        int day = 0;
                        switch (month)
                        {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                day = 31;
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                day = 30;
                                break;
                            case 2:
                                if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0))
                                {
                                    day = 29;
                                }
                                else
                                {
                                    day = 28;
                                }
                                break;
                        }
                        Console.WriteLine("{0}年{1}月有{2}天", year, month, day);
                    }
                }
                catch
                {
                    Console.WriteLine("输入的月份有误，程序退出");
                }
            }
            catch
            {
                Console.WriteLine("输入的年份有误，程序退出");
            }
        }
    }
}