﻿namespace _24switch_case练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：让用户输入姓名，然后显示出这个人上辈子是什么职业.
            #region 答案
            Console.WriteLine("请输入一个姓名，我们将显示出这个人上辈子的职业");
            string name = Console.ReadLine();
            switch (name)
            {
                case "老杨": Console.WriteLine("上辈子是抽大烟的");
                    break;
                case "老苏": Console.WriteLine("上辈子是个老旧");
                    break;
                case "老蒋": Console.WriteLine("上辈子是国民党");
                    break;
                case "老牛": Console.WriteLine("上辈子是个老牛");
                    break;
                case "老虎": Console.WriteLine("上辈子是个病猫");
                    break;
                default: Console.WriteLine("不认识，估计是一坨翔");
                    break;
            }
            #endregion

            /*练习2：对学员的结业考试成绩评测
             * 成绩>=90：A
             * 90>成绩>=80：B
             * 80>成绩>=70：C
             * 70>成绩>=60：D
             * 成绩 < 60：E
             */
            #region 答案
            Console.WriteLine("请输入一个考试成绩");
            int score = Convert.ToInt32(Console.ReadLine());
            //将范围变成一个定值。
            switch (score / 10)
            {
                case 9: Console.WriteLine("A");
                    break;
                case 8: Console.WriteLine("B");
                    break;
                case 7: Console.WriteLine("C");
                    break;
                case 6: Console.WriteLine("D");
                    break;
                default: Console.WriteLine("E");
                    break;
            }
            #endregion
        }
    }
}