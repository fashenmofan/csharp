﻿namespace _53自己手写tryparse
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            bool flag = int.TryParse("123", out num); //转换失败为0
            Console.WriteLine(num);
            Console.WriteLine(flag);
        }

        public static bool MyTryParse(string s, out int result)
        {
            result = 0;
            try
            {
                result = Convert.ToInt32(s);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}