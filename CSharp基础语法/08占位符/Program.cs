﻿namespace _08占位符
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1 = 10;
            int n2 = 20;
            int n3 = 30;
            Console.WriteLine("第一个数字是" + n1 + ",第二个数字是" + n2 + ",第三个数字是" + n3);
            //占位符的使用方便书写，思路清晰。
            Console.WriteLine("第一个数字是{0},第二个数字是{1},第三个数字是{2}", n1, n2, n3);
            Console.ReadKey();

            #region 占位符练习
            //3. 定义四个变量, 分别存储一个人的姓名、性别(Gender)、年龄、电话
            // (TelephoneNumber)。然后打印在屏幕上（我叫X,我今年  X  岁了,我是X生,
            //我的电话是  X X  ) (电话号用什么类型, 如: 010-12345)
            string name = "李四";
            char gender = '男';
            int age = 18;
            string tel = "12341425253";
            Console.WriteLine("我叫{0}，我今年{1}岁了，我是一名{2}生，我的电话号码是{3}", name, age, gender, tel);
            #endregion
        }
    }
}