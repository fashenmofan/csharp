﻿namespace _01注释符的使用
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.解释代码
            //这行代码的作用是将Hello World打印到控制台当中
            Console.WriteLine("Hello World");

            //2.注销代码
            //Console.ReadKey();

            //多行注释
            /*Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");*/

            #region 这些代码是我没用的但是去不想删的代码
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            Console.WriteLine("Hello World");
            #endregion

        }

        /// <summary>
        /// 这个方法的作用就是求两个整数之间的最大值
        /// </summary>
        /// <param name="n1">第一个整数</param>
        /// <param name="n2">第二个整数</param>
        /// <returns>返回比较大的那个数字</returns>
        public static int GetMax(int n1, int n2)
        {
            return n1 > n2 ? n1 : n2;
        }
    }

    /// <summary>
    /// 这个类用来表述一个人的信息，从姓名 性别 年龄表述
    /// </summary>
    public class Person
    {
        public string Name
        {
            get;
            set;
        }
    }
}