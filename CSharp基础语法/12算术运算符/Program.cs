﻿namespace _12算术运算符
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1 = 10;
            int n2 = 3;
            int result = n1 / n2;
            Console.WriteLine(result);

            #region 演示：某学生三门课成绩为 语文:90，数学:80，英语:67，编程求总分和平均分。
            int Chinese = 90;
            int math = 88;
            int English = 67;
            Console.WriteLine("总成绩是{0}，平均成绩是{1}", Chinese + math + English, (Chinese + math + English) / 3);
            #endregion

            #region 练习1：计算半径为 5 的圆的面积和周长并打印出来. (pi为 3.14) 面积: pirr; Perimeter(周长)
            int r = 5;
            double area = 3.14 * r * r;
            double perimeter = 2 * 3.14 * r;
            Console.WriteLine("圆的面积是{0}，周长是{1}", area, perimeter);
            #endregion

            #region 练习2: 某商店T恤(T - shirt)的价格为35元 / 件,裤子(trousers)的价格为 120 元 / 条.小明在该店买了3件T恤和2条裤子, 请计算并显示小明应该付多少钱?打 8.8折后呢 ?
            int T_shirt = 35;
            int trousers = 120;
            int totalMoney = 3 * T_shirt + 2 * trousers;
            Console.WriteLine("需付{0}", totalMoney);
            double realMoney = totalMoney * 0.88;
            Console.WriteLine("实付{0}", realMoney);
            #endregion
        }
    }
}