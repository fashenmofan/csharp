﻿namespace _41枚举类型和int以及string类型之间的转换
{
    public enum QQstate
    {
        OnLine=2,
        OffLine,
        Leave=5,
        Busy,
        QMe
    }

    public enum Exercise
    {
        OnLine = 2,
        OffLine,
        Leave = 5,
        Busy,
        QMe
    }
    class Program
    {
        static void Main(string[] args)
        {
            #region 将枚举类型强转成int类型
            //QQstate qstate = QQstate.OnLine;
            ////枚举类型默认可以跟int类型相互转换，枚举类型跟int类型是兼容的
            //int n = (int)qstate;
            //Console.WriteLine(n);
            //Console.WriteLine((int)QQstate.OffLine);
            //Console.WriteLine((int)QQstate.Leave);
            //Console.WriteLine((int)QQstate.Busy);
            //Console.WriteLine((int)QQstate.QMe);
            #endregion

            #region 将int类型强转枚举类型
            //int n = 3;
            //QQstate qstate = (QQstate)n;
            //Console.WriteLine(qstate); //OffLine
            //n = 4;
            //qstate = (QQstate)n;
            //Console.WriteLine(qstate); //4，因为枚举中不存在该值。
            #endregion

            #region 将枚举类型转换成字符串类型
            //所有类型都能够转换成string类型

            //int n = 10;
            //double n = 3.14;
            //decimal n = 20000m;
            //QQstate n = QQstate.Busy;
            //string s = n.ToString();
            //Console.WriteLine(s);
            #endregion

            #region 将字符串类型转换成枚举类型
            //将s转换成枚举类型
            //Convert.ToInt32() int.parse() int.TryParse()
            //调用Parse()方法的目的就是为了让它帮助我们将一个字符串转换成对应的枚举类型
            //typeof() 拿到该变量的类型

            //string s1 = "0";
            //QQstate qstate = (QQstate)Enum.Parse(typeof(QQstate), s1);
            //Console.WriteLine(qstate); //0
            //s1 = "2";
            //qstate = (QQstate)Enum.Parse(typeof(QQstate), s1);
            //Console.WriteLine(qstate); //OnLine
            //s1 = "ABC"; //当遇到没有的值时转换抛异常
            //qstate = (QQstate)Enum.Parse(typeof(QQstate), s1);
            //Console.WriteLine(s1);
            #endregion




            //枚举练习
            //提示用户选择一个在线状态，我们接受，并将用户的输入转换成枚举类型
            //再次打印到控制台中
            Console.WriteLine("请选择您的qq在线状态 1--Online 2--OffLine 3--Leave 4--Busy 5--QMe");
            string input = Console.ReadLine();
            switch (input)
            {
                case "1": Exercise s1 = (Exercise)Enum.Parse(typeof(Exercise), input);
                    Console.WriteLine("您选择的在线状态是{0}", s1);
                    break;
                case "2": Exercise s2 = (Exercise)Enum.Parse(typeof(Exercise), input);
                    Console.WriteLine("您选择的在线状态是{0}", s2);
                    break;
                case "3":
                    Exercise s3 = (Exercise)Enum.Parse(typeof(Exercise), input);
                    Console.WriteLine("您选择的在线状态是{0}", s3);
                    break;
                case "4":
                    Exercise s4 = (Exercise)Enum.Parse(typeof(Exercise), input);
                    Console.WriteLine("您选择的在线状态是{0}", s4);
                    break;
                case "5":
                    Exercise s5 = (Exercise)Enum.Parse(typeof(Exercise), input);
                    Console.WriteLine("您选择的在线状态是{0}", s5);
                    break;
            }
        }
    }
}