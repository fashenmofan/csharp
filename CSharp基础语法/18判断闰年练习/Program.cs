﻿namespace _18判断闰年练习
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             写下判断闰年的表达式, 设待判断的年份 变量为year.
             闰年的判定(符合下面两个条件之一):
                1.年份能够被 400 整除.(2000)
                2.年份能够被4整除但不能被100整除
             让用户输入一个年份, 如果是闰年, 则输出 true, 如果不是, 则输出false.
             2100/1600/1800/2009年是闰年吗?
             */
            #region 答案
            Console.WriteLine("请输入要判断的年份");
            int year = Convert.ToInt32(Console.ReadLine());
            //这样写也正确是因为逻辑与的优先级要高于逻辑或
            //bool b = year % 400 == 0 || year % 4 == 0 && year % 100 != 0;
            bool b = (year % 400 == 0) || (year % 4 == 0 && year % 100 != 0);
            Console.WriteLine(b);
            #endregion
        }
    }
}