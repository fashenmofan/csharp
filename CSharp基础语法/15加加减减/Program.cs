﻿namespace _15加加减减
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 10;
            int result = 10 + number++;
            Console.WriteLine(number);
            Console.WriteLine(result);
            Console.WriteLine();
            number = 10;
            result = 10 + ++number;
            Console.WriteLine(number);
            Console.WriteLine(result);
            Console.WriteLine();
            number = 10;
            result = 10 + number--;
            Console.WriteLine(number);
            Console.WriteLine(result);
            Console.WriteLine();
            number = 10;
            result = 10 + --number;
            Console.WriteLine(number);
            Console.WriteLine(result);
            Console.WriteLine();

            //你觉得你听明白了？
            int a = 5;
            int b = a++ + ++a * 2 + --a + a++;
            //  b = 5   +   7 * 2 +   6 + 6
            //  a = 6       7         6   7
            Console.WriteLine(a);
            Console.WriteLine(b);
        }
    }
}