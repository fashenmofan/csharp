﻿namespace _34类型转换_Parse
{
    class Program
    {
        static void Main(string[] args)
        {
            //使用Convert进行转换 成功了就成了，失败了就抛异常
            //int number = Convert.ToInt32("123abc");

            //Convert底层就是使用了Parse，所以Parse也会抛异常。
            //int.Parse("123abc");
            //double.Parse("123abc");
            //decimal.Parse("123abc");

            //TryParse是一个方法或者函数
            int number = 100;
            Console.WriteLine("123是否转换成功？");
            bool flag = int.TryParse("123", out number);
            Console.WriteLine("{0}, 这个数是{1}", flag, number);
        }
    }
}
