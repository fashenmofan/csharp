﻿namespace _50方法练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：读取输入的整数, 定义成方法, 多次调用
            //(如果用户输入的是数字，则返回，否则提示用户重新输入)
            #region 答案
            //Console.WriteLine("请输入一个数字");
            //string input = Console.ReadLine();
            //int number = GetNumber(input);
            //Console.WriteLine(number);
            #endregion

            //练习2：还记得学循环时做的那道题吗？
            //只允许用户输入y或n，请改成方法
            #region 答案
            //Console.WriteLine("请输入yes或者no");
            //string input = Console.ReadLine();
            //string result = IsYesOrNo(input);
            //Console.WriteLine(result);
            #endregion

            //练习3：计算输入数组的和: int Sum(int[] values)
            #region 答案
            int[] nums = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int sum = GetSum(nums);
            Console.WriteLine(sum);
            #endregion
        }

        /// <summary>
        /// 这个方法需要判断用户的输入是否是数字
        /// 如果是数字，则返回
        /// 如果不是数字，提示用户重新输入
        /// </summary>
        /// <param name="s">输入的字符串</param>
        /// <returns>将字符串转换出来的数字</returns>
        public static int GetNumber(string s)
        {
            while (true)
            {
                try
                {
                    int number = Convert.ToInt32(s);
                    return number;
                }
                catch
                {
                    Console.WriteLine("输入有误！！！");
                    s = Console.ReadLine();
                }
            }
        }

        /// <summary>
        /// 限定用户只能输入yes或者no 并且返回
        /// </summary>
        /// <param name="input">用户的输入</param>
        /// <returns>返回yes或者no</returns>
        public static string IsYesOrNo(string input)
        {
            while (true)
            {
                if (input == "yes" || input == "no")
                {
                    return input;
                }
                else
                {
                    Console.WriteLine("只能输入yes或者no，请重新输入");
                    input = Console.ReadLine();
                }
            }
        }

        /// <summary>
        /// 计算一个整数类型的总和
        /// </summary>
        /// <param name="nums">要求总和的数组</param>
        /// <returns>返回这个数组的总和</returns>
        public static int GetSum(int[] nums)
        {
            int sum = 0;
            for (int i = 0; i < nums.Length; i ++)
            {
                sum += nums[i];
            }
            return sum;
        }
    }
}