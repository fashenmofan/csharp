﻿namespace _35for循环的3个练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：循环录入5个人的年龄并计算平均年龄
            //如果录入的数据出现负数或大于100的数，立即停止输入并报错。
            #region 答案
            int sum = 0;
            bool flag = true;
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("请输入{0}个人的成绩", i + 1);
                try
                {
                    int age = Convert.ToInt32(Console.ReadLine());
                    if (age >= 0 && age <= 100)
                    {
                        sum += age;
                    }
                    else
                    {
                        Console.WriteLine("输入的年龄不在正确范围内，程序退出！！！");
                        flag = false;
                        break;
                    }
                }
                catch
                {
                    Console.WriteLine("输入的年龄不正确，程序退出！！！");
                    flag = false;
                    break;
                }
            }
            if (flag)
                Console.WriteLine("五个人的平均年龄是{0}", sum / 5);
            #endregion

            //练习2：在while中用break实现要求用户一直输入用户名和密码，
            //只要不是admin、888888就一直提示要求重新输入，如果正确则提示登录成功
            #region 答案
            string name = "";
            string pwd = "";
            while (true)
            {
                Console.WriteLine("请输入用户名");
                name = Console.ReadLine();
                Console.WriteLine("请输入密码");
                pwd = Console.ReadLine();

                if (name == "admin" && pwd == "888888")
                {
                    Console.WriteLine("登录成功");
                    break;
                }
                else
                {
                    Console.WriteLine("用户名或密码错误，请重新输入");
                }
            }
            #endregion

            //练习3：1-100之间的整数相加，得到累加值大于20的当前数
            //(比如:1+2+3+4+5+6=21)结果6 sum>=20 i
            #region 答案
            sum = 0;
            for (int i = 1; i <= 100; i++)
            {
                sum += i;
                if (sum >= 20)
                {
                    Console.WriteLine("加到{0}的时候，总和就大于20", i);
                    break;
                }
            }
            #endregion

            //练习4：找出100内所有的素数
            //素数/质数：只能被1和这个数字本身整除的数字
            //最小的质数是2
            #region 答案
            for (int i = 2; i < 100; i++)
            {
                flag = true;
                for (int j = 2; j * j < i; j++)
                {
                    if (i % j == 0)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    Console.WriteLine(i);
            }
            #endregion
        }
    }
}