﻿namespace _02变量以及基本数据类型
{
    class Program
    {
        static void Main(string[] args)
        {
            //变量类型 变量名;
            //变量名 = 值;

            //官方语言：声明或者定义了一个int类型的变量
            int number; //在内存中开辟一块能够存储整数的空间
            //官方语言：给这个变量进行赋值
            number = 100; //表示把100存储到了这块空间内


            double d = 3.0;

            //String是一个类，String和string编译之后最终都会映射到CS
            //String属于.Net平台的类型；string独属于CSharp中的类型，是关键字
            //为什么会有关键字呢？因为string好写。
            String str;
            string lsName = "李四";
            string s = "";//字符串可以存储 空

            //字符串和字符的区别，羊肉串和羊肉
            char gender = '男';
            //char gender = '男女';

            decimal money = 5000m;
        }
    }
}