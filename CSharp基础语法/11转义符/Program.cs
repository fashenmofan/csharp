﻿namespace _11转义符
{
    class Program
    {
        static void Main(string[] args)
        {
            char c = '\b'; // \在里面起到了一个转义的作用
            //char cc = 'bb'; //报错
            Console.WriteLine(c);

            // \n:表示换行
            Console.WriteLine("今天天气好晴朗\n处处好风光\n");
            // \":表示一个英文半角的双引号
            Console.WriteLine("我想在这句话中输出一个英文半角的双引号\"");

            string name1 = "李四";
            string name2 = "李敏镐死都";
            string name3 = "井边席上创下枕头编剧";
            string name4 = "isnfsjfisffd";
            // \t:表示一个tab键的空格
            //使用多个\t，因为名字不一样长。
            Console.WriteLine("{0}\t\t\t{1}", name1, name2);
            Console.WriteLine("{0}\t{1}", name3, name4);

            // \b:表示一个退格键，放在字符串两边没有效果
            Console.WriteLine("\b学习编程不一定学会\b，学会了不一定能够找到工作\b，找到工作不一定能够买得起房子\b，买得起房子不一定能够娶到老婆\b");

            // 以前windows不能够识别\n，需要使用\r\n才能够改变，现在windows能够识别\n了
            string str = "今天天气好风光\n处处好风光";
            System.IO.File.WriteAllText(@"C:\Users\不屈的夏天\Desktop\111.txt", str);
            Console.WriteLine("写入成功!!!");

            //像路径中会存在很多\字符 会导致报错，故我们可以通过再次添加\避免被转义 
            string path = "F:\\老赵生活\\music\\a\\b\\c\\d\\e\\雄安则玛丽亚.avi";
            //但是path中的\往往很多，故提出了@符号可以取消\在字符串中的转义作用。
            string path1 = @"F:\老赵生活\music\a\b\c\d\e\雄安则玛丽亚.avi";
            //string path1 = "F:\老赵生活\music\a\b\c\d\e\雄安则玛丽亚.avi"; //报错
            Console.WriteLine(path);

            //@符号保留原格式输出
            Console.WriteLine(@"今天天气好清凉
                处处好风光");
            //报错
            //Console.WriteLine("今天天气好清凉
            //    处处好风光");
            Console.ReadKey();
        }
    }
}