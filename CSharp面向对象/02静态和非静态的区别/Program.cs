﻿namespace _02静态和非静态的区别
{
    class Progame
    {
        static void Main(string[] args)
        {
            //调用实例成员
            Person person = new Person();
            person.M1(); //实例方法
            Person.M2(); //静态方法
            Console.WriteLine(); //静态方法

            //Student s = new Student(); //静态类不允许实例化
        }
    }
}