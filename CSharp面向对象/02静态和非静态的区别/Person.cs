﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02静态和非静态的区别
{
    public class Person
    {
        private static string name;

        private char _gender;

        public void M1()
        {
            Console.WriteLine("我是非静态的方法");
        }

        public static void M2()
        {
            Console.WriteLine("我是一个静态方法");
        }
    }
}
