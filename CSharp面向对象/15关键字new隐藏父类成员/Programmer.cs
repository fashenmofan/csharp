﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15关键字new隐藏父类成员
{
    public class Programmer : Person
    {
        private int _workYear;

        public int WorkYear
        {
            get { return _workYear; }
            set { _workYear = value; }
        }

        public void ProgrammerSayHello()
        {
            Console.WriteLine("我是{0}，我是一名程序猿，我的年龄是{1}，我是{2}生，我的工作年限是{3}年。", this.Name, this.Age, this.Gender, this.WorkYear);
        }

        public Programmer(string name, int age, char gender, int workYear)
            :base(name, age, gender)
        {
            this.Name = name;
            this.Age = age;
            this.Gender = gender;
            this.WorkYear = workYear;
        }

        public new void SayHello()
        {
            Console.WriteLine("大家好，我是程序员");
        }
    }
}
