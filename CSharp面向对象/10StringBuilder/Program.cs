﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10StringBuilder
{
    class Program
    {
        public static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder(); //耗时 00:00:00.0044141
            string str = ""; //耗时 00:00:15.5452216

            //创建了一个计时器，用来记录程序运行的时间
            Stopwatch sw = new Stopwatch();
            sw.Start(); //开始计时
            for (int i = 0; i < 100000; i ++)
            {
                //str += i;
                sb.Append(i);
            }
            sw.Stop(); //结束计时
            Console.WriteLine(sw.Elapsed);
            Console.WriteLine(sb.ToString());
            Console.ReadKey();
        }
    }
}