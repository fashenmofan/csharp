﻿namespace _13继承
{
    public class Program
    {
        static void Main(string[] args)
        {
            Student stu = new Student("你好", 18, '男', 160016);
            stu.CHLSS();
            stu.Study();
            stu.Name = "年后";
            Console.WriteLine(stu.Name);
        }
    }

    //不适用于开发，每个.cs文件中只能存在一个类
    //这里是为了方便学习
    public class Person
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _age;
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        private char _gender;
        public char Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public void CHLSS()
        {
            Console.WriteLine("吃喝拉撒睡");
        }

        public Person(string name, int age, char gender)
        {
            this.Name = name;
            this.Age = age;
            this.Gender = gender;
        }

        //public Person()
        //{

        //}
    }

    public class Student : Person
    {
        //private string _name;

        //public string Name
        //{
        //    get { return _name; }
        //    set { _name = value; }
        //}

        //private int _age;
        //public int Age
        //{
        //    get { return _age; }
        //    set { _age = value; }
        //}

        //private char _gender;
        //public char Gender
        //{
        //    get { return _gender; }
        //    set { _gender = value; }
        //}

        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        //public void CHLSS()
        //{
        //    Console.WriteLine("吃喝拉撒睡");
        //}

        public void Study()
        {
            Console.WriteLine("学生会学习");
        }

        public Student(string name, int age, char gender, int id)
            :base(name, age, gender)
        {
            this.Name = name;
            this.Age = age;
            this.Gender = gender;
            this.Id = id;
        }
    }

    public class Teacher : Person
    {
        //private string _name;

        //public string Name
        //{
        //    get { return _name; }
        //    set { _name = value; }
        //}

        //private int _age;
        //public int Age
        //{
        //    get { return _age; }
        //    set { _age = value; }
        //}

        //private char _gender;
        //public char Gender
        //{
        //    get { return _gender; }
        //    set { _gender = value; }
        //}

        private double _salary;
        public double Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        //public void CHLSS()
        //{
        //    Console.WriteLine("吃喝拉撒睡");
        //}

        public void Teach()
        {
            Console.WriteLine("老师会讲课");
        }

        public Teacher(string name, int age, char gender, int salary)
        : base(name, age, gender)
        {
            this.Name = name;
            this.Age = age;
            this.Gender = gender;
            this.Salary = salary;
        }
    }

    public class Driver : Person
    {
        //private string _name;
        //public string Name
        //{
        //    get { return _name; }
        //    set { _name = value; }
        //}

        //private int _age;
        //public int Age
        //{
        //    get { return _age; }
        //    set { _age = value; }
        //}

        //private char _gender;

        //public char Gender
        //{
        //    get { return _gender; }
        //    set { _gender = value; }
        //}

        private int _driveTime;
        public int DriveTime
        {
            get { return _driveTime; }
            set { _driveTime = value; }
        }

        //public void CHLSS()
        //{
        //    Console.WriteLine("吃喝拉撒睡");
        //}

        public void Drive()
        {
            Console.WriteLine("司机会开车");
        }

        public Driver(string name, int age, char gender, int driveTime)
        : base(name, age, gender)
        {
            this.Name = name;
            this.Age = age;
            this.Gender = gender;
            this.DriveTime = driveTime;
        }
    }
}