﻿using _06面向对象初级复习;

namespace _07命名空间using
{
    class Program
    {
        public static void Main(string[] args)
        {
            List<int> list = new List<int>();
            //Alt + Enter

            Person zxyPerson = new Person();
            zxyPerson.Name = "张三";
            Console.WriteLine(zxyPerson.Name);
            Console.ReadKey();
        }
    }
}