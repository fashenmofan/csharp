﻿namespace _05面向对象练习
{
    class Program
    {
        public static void Main(string[] args)
        {
            //练习:
            //写一个Ticket类，有一个距离属性（本属性只读，在构造方法中赋值）
            //不能为负数，有一个价格属性，价格属性只读，
            //并且根据距离distance计算价格Price（1元/公里）:
            //0~100公里           票价不打折
            //101~300公里         总额打9.5折
            //201~300公里         总额打9折
            //300公里以上         总额打8折
            Ticket ticket = new Ticket(500);
            ticket.ShowTicket();
            Console.ReadKey();
        }
    }
}