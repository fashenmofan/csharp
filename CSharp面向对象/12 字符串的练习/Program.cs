﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12字符串练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //练习1：接受用户输入的字符串，将其中的字符以输入相反的顺序输出。"abc"→"cba"
            #region 答案
            //string str = "abcdefg";
            //char[] chs = str.ToCharArray();
            //for (int i = 0; i < chs.Length / 2; i ++)
            //{
            //    char temp = chs[i];
            //    chs[i] = chs[chs.Length - 1 - i];
            //    chs[chs.Length - 1 - i] = temp; 
            //}
            //str = new string(chs);
            //Console.WriteLine(str);
            //Console.ReadKey();
            #endregion

            //练习2："hello c sharp" → "sharp c hello"
            #region 答案
            //string str = "hello c sharp";
            //string[] strNew = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            //for (int i = 0; i < strNew.Length / 2; i ++)
            //{
            //    string temp = strNew[i];
            //    strNew[i] = strNew[strNew.Length - 1 - i];
            //    strNew[strNew.Length - i - 1] = temp;
            //}
            //str = string.Join(' ', strNew);
            //Console.WriteLine(str);
            ////for (int i = 0; i < strNew.Length; i ++)
            ////{
            ////    Console.WriteLine(strNew[i]);
            ////}
            //Console.ReadKey();
            #endregion

            //练习3：从Email中提取出用户名和域名：abc@163.com。
            #region 答案
            string email = "abc@163.com";
            //方法一
            //string[] email1 = email.Split('@');
            //for (int i = 0; i < email1.Length; i ++)
            //{
            //    Console.WriteLine(email1[i]);
            //}

            //方法二
            //int index = email.IndexOf('@');
            //string userName = email.Substring(0, index);
            //string org = email.Substring(index);
            //Console.WriteLine(userName);
            //Console.WriteLine(org);
            //Console.ReadKey();
            #endregion

            //练习4：文本文件中存储了多个文章标题、作者，
            //标题和作者之间用若干空格（数量不定）隔开，每行一个，
            //标题有的长有的短，输出到控制台的时候最多标题长度 10 ，
            //如果超过 10 ，则截取长度8的子串并且最后添加“...”，加一个竖线后输出作者的名字。
            #region 答案
            //string path = @"D:\CSharp\CSharp面向对象\12 字符串的练习\文本文件.txt";
            //string[] contents =  File.ReadAllLines(path, Encoding.Default);
            //for (int i = 0; i < contents.Length; i ++)
            //{
            //    string[] strNew = contents[i].Split(new char[] {}, StringSplitOptions.RemoveEmptyEntries);
            //    Console.WriteLine((strNew[0].Length > 10 ? strNew[0].Substring(0, 8) + "..." : strNew[0]) + "|" + strNew[1]);
            //}
            //Console.ReadKey();
            #endregion

            //练习5：让用户输入一句话，找出所有e的位置
            #region 答案
            //string str = "abcdefjehigklmenopqrseuevwxeyze";

            // 方法一 适用于找多个字符或者单个字符
            //int cnt = 1;
            //int index = str.IndexOf('e');
            //Console.WriteLine("第{0}次出现e的位置是{1}", cnt, index + 1);
            //cnt++;
            //while (index != -1)
            //{
            //    index = str.IndexOf('e', index + 1);
            //    if (index == -1) break; 
            //    Console.WriteLine("第{0}次出现e的位置是{1}", cnt, index);
            //    cnt ++;
            //}
            //Console.ReadKey();

            //方法二
            //for (int i = 0; i < str.Length; i++)
            //{
            //    if (str[i] == 'e')
            //    {
            //        Console.WriteLine(i);
            //    }
            //}
            //Console.ReadKey();
            #endregion

            //练习6：用户输入一句话，判断这句话中有没有邪恶，如果有邪恶就替换成这种形式然后输出，如：老牛很邪恶
            #region 答案
            //string str = "老牛很邪恶";
            //if (str.Contains("邪恶"))
            //{
            //    str = str.Replace("邪恶", "**");
            //}
            //Console.WriteLine(str);
            //Console.ReadKey();
            #endregion

            //练习7：把{"诸葛亮", "鸟叔", "卡卡西", "卡哇伊"}变成诸葛亮|鸟叔|卡卡西|卡哇伊，然后再把|切割掉
            #region 答案
            string[] names = { "诸葛亮", "鸟叔", "卡卡西", "卡哇伊" };
            string str = string.Join('|', names);
            string[] strNew = str.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < strNew.Length; i ++)
            {
                Console.WriteLine(strNew[i]);
            }
            Console.WriteLine(str);
            Console.ReadKey();
            #endregion
        }
    }
}