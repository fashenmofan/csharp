﻿namespace _03类的练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //两遍初始化对象，代码很繁琐
            Student zsStudent = new Student();
            zsStudent.Name = "张三";
            zsStudent.Age = 18;
            zsStudent.Gender = '中';
            zsStudent.Chinese = 100;
            zsStudent.Math = 100;
            zsStudent.English = 100;
            zsStudent.SayHello();
            zsStudent.ShowScore();

            Student lsStudent = new Student();
            lsStudent.Name = "李四";
            lsStudent.Age = 18;
            lsStudent.Gender = '女';
            lsStudent.Chinese = 90;
            lsStudent.Math = 90;
            lsStudent.English = 90;
            zsStudent.SayHello();
            zsStudent.ShowScore();
        }
    }
}