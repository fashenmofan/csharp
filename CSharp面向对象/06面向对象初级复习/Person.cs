﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06面向对象初级复习
{
    public class Person
    {
        //字段、属性、方法、构造函数
        //字段:存储数据
        //属性:保护字段，对字段的取值和设值进行限定
        //方法:描述对象的行为
        //构造函数:初始化对象(给对象的每个属性依次赋值)
        //类中的成员，如果不加访问修饰符，默认都是private
        string _name;
        /// <summary>
        /// 属性的本质就是两个方法
        /// </summary>
        public string Name
        {
            get { return _name; }
            set {
                if (value != "孙权")
                {
                    value = "孙权";
                }
                _name = value; }
        }

        int _age;
        public int Age
        {
            get {
                if (_age < 0 || _age > 150)
                {
                    return _age = 0;
                }
                return _age; }
            set { _age = value; }
        }

        char _gender;
        public char Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        //this:当前类的对象
        //this:调用当前类的构造函数
        public void SayHello()
        {
            string Name = "李四"; //this区分局部变量和属性
            Console.WriteLine("{0},{1},{2}", Name, this.Age, this.Gender);
        }

        //静态函数只能够访问到静态成员
        public static void SayHelloTwo()
        {
            Console.WriteLine("Hello 我是静态的方法");
        }

        //构造函数: 1、没有返回值 void返回值
        //         2、构造函数的名称跟类名一样
        public Person(string name, int age, char gender)
        {
            this.Name = name;
            this.Age = age;
            if (gender != '男' && gender != '女')
            {
                gender = '男';
            }
            this.Gender = gender;
        }

        public Person(string name, char gender):this(name, 0, gender)
        {
            //this.Name = name;
            //this.Gender = gender;
        }

        public Person()
        {

        }
    }
}

