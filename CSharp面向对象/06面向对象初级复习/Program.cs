﻿namespace _06面向对象初级复习
{
    class Program
    {
        public static void Main(string[] args)
        {
            Person zxyPerson = new Person("张三", 18, '男');
            zxyPerson.SayHello();
            Console.ReadKey();
            //new
            //1.在内存中开辟一块空间
            //2.在开辟的空间中创建对象
            //3.调用对象的构造函数

            Person.SayHelloTwo();
        }
    }
}
