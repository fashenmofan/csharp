﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14继承练习
{
    public class Reporter:Person
    {
        private string _hobby;

        public string Hobby
        {
            get { return _hobby; }
            set { _hobby = value; }
        }

        public void ReporterSayHello()
        {
            Console.WriteLine("我叫{0}，我是一个狗仔，我的爱好是{1}，我的年龄是{2}，我是一个{3}狗仔。", this.Name, this.Hobby, this.Age, this.Gender);
        }

        public Reporter(string name, int age, char gender, string hobby)
            :base(name, age, gender)
        {
            this.Name = name;
            this.Age = age;
            this.Gender = gender;
            this.Hobby = hobby;
        }
    }
}
