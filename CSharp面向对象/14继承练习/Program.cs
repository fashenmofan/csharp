﻿namespace _14继承练习
{
    class Program
    {
        static void Main(string[] args)
        {
            //记者:我是一个狗仔，我的爱好是偷拍，我的年龄是34，我是一个男狗仔。
            //司机:我叫舒马赫，我的年龄是43，我是男人，我的驾龄是23年。
            //程序员:我是孙权，我的年龄是23，我是男生，我的工作年限是3年。
            Reporter reporter = new Reporter("狗仔", 34, '男', "偷拍");
            reporter.ReporterSayHello();
            Programmer programmer = new Programmer("程序员", 23, '男', 3);
            programmer.ProgrammerSayHello();
            Console.ReadKey();
        }
    }
}