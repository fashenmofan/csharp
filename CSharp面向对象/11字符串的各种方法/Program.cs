﻿namespace _11字符串的各种方法
{
    class Program
    {
        public static void Main(string[] args)
        {
            //练习一: 随机输入你心中想到的一个名字，然后输出它的字符串长度
            //Length: 可以得到字符串的长度
            #region 答案
            //Console.WriteLine("请输入你心中想的那个人的名字");
            //string name = Console.ReadLine();
            //Console.WriteLine("心中想的人的长度是{0}", name.Length);
            //Console.ReadKey(true);
            #endregion

            //ToUpper(): 将字符串转换成大写
            //ToLower(): 将字符串转换成小写
            //Equals(lessonTwo, StringComparison.OrdinalIgnoreCase)：比较两个字符串并且忽略大小写
            #region 示例
            //Console.WriteLine("请输入你喜欢的课程");
            //string lessonOne = Console.ReadLine();
            //Console.WriteLine("请输入你喜欢的课程");
            //string lessonTwo = Console.ReadLine();
            ////lessonOne = lessonOne.ToLower();
            ////lessonTwo = lessonTwo.ToLower();
            ////lessonOne = lessonOne.ToUpper();
            ////lessonTwo = lessonTwo.ToUpper();

            //if (lessonOne.Equals(lessonTwo, StringComparison.OrdinalIgnoreCase))
            //{
            //    Console.WriteLine("你们两个喜欢的课程相同哦");
            //}
            //else
            //{
            //    Console.WriteLine("你们两个喜欢的课程不相同");
            //}
            //Console.ReadKey(true);
            #endregion

            //分割字符串Split
            #region 示例
            //string s = "a b dfs _ sdff sdf,, dsf = fg ";
            //char[] chs = {' ', '_', '+', ',', '=' };
            ////string[] str =  s.Split(chs); //只是分割字符串但还是将chs中的字符保留了。
            //string[] str = s.Split(chs, StringSplitOptions.RemoveEmptyEntries);
            //for (int i = 0; i < str.Length; i++)
            //    Console.WriteLine(str[i]);
            //Console.ReadKey(true);
            #endregion

            //练习二：从日期字符串("2008-08-08")中分析出年、月、日；2008年08月08日.
            //让用户输入一个日期格式如:2008-01-02，你输出你输入的日期为2008年1月2日
            #region 答案
            //string s = "2008-08-08";
            //char[] chs = { '-' };
            //string[] str = s.Split(chs, StringSplitOptions.RemoveEmptyEntries);
            //Console.WriteLine("{0}年{1}月{2}日", str[0], str[1], str[2]);
            //Console.ReadKey();
            #endregion

            //bool Contains(string value)判断字符串中是否含有子串value
            //字符串替换：string Replace(string oldValue, string newValue)将字符串出现oldValue的地方替换为newValue
            //应用场景：和谐一些关键字
            #region
            //string str = "国家关键人物老李";
            //if (str.Contains("老李"))
            //{
            //    str = str.Replace("老李", "**");
            //}
            //Console.WriteLine(str);
            //Console.ReadKey();
            #endregion

            //string Substring(...) 截取字符串
            #region
            //string str = "今天天气晴朗，处处好风光";
            ////str = str.Substring(0);
            ////str = str.Substring(1); //包含索引为1的字节
            //str = str.Substring(2, 4); //从索引2开始的四个字符
            //Console.WriteLine(str);
            //Console.ReadKey();
            #endregion

            //bool StartsWith(string value) 判断字符串是否以子串value开始
            //bool EndsWith(string value) 判断字符串是否以子串value结束
            #region
            //string str = "今天天气晴朗，处处好风光";
            //if (str.StartsWith("今天"))
            //{
            //    Console.WriteLine("是的");
            //}
            //else
            //{
            //    Console.WriteLine("不是的");
            //}
            //Console.ReadKey();
            #endregion

            //IndexOf(string value) 取子串value第一次出现的位置
            //IndexOf(string value, int startindex) 从startindex开始寻找
            #region
            //string str = "今天天气晴朗，天处处好风光";
            ////int index = str.IndexOf('天'); //1
            //int index = str.IndexOf('天', 3); //7
            ////int index = str.IndexOf('哈'); //-1
            //Console.WriteLine(index);
            //Console.ReadKey();
            #endregion

            //LastIndexOf 取子串value最后一次出现的位置
            #region
            //string str = "今天天气晴朗，处处好风光";
            //int index = str.LastIndexOf('天');
            //Console.WriteLine(index);
            //Console.ReadKey();
            #endregion

            //LastIndexOf应用场景
            //和Substring一起使用获取文件路径中的名称
            #region
            //string path = @"C:\sd\sg\sgfg\sfdbh\sfg全fg\f全dfhhfdg\全职法师.pdf";
            //int index = path.LastIndexOf('\\'); //两个斜杠 转义斜杠
            //path = path.Substring(index + 1);
            //Console.WriteLine(path);
            //Console.ReadKey();
            #endregion

            //string Trim() 去除前后空格
            //string TrimStart() 去除前面空格
            //string TrimEnd() 去除后面空格
            #region
            //string str = "      hahaha             ";
            ////str = str.Trim();
            ////str = str.TrimStart();
            //str = str.TrimEnd();
            //Console.WriteLine(str);
            //Console.ReadKey();
            #endregion

            //string.IsNullOrEmpty()  如果value参数为空或空字符串("")则为True;否则,假的。
            #region
            //string str = "";
            ////string str = null;
            ////string str = "fgfg";
            //if (string.IsNullOrEmpty(str))
            //{
            //    Console.WriteLine("是空或者null");
            //}
            //else
            //{
            //    Console.WriteLine("不是空或者null");
            //}
            //Console.ReadKey();
            #endregion

            //string.Join(char separator, params object?[] values) 返回：由分隔符分隔的值的元素组成的字符串。
            #region
            //string[] names = { "张三", "李四", "王五", "赵六", "田七" };
            ////张三|李四|王五|赵六|田七
            //string strNew = string.Join("|", names);
            //Console.WriteLine(strNew);
            //Console.ReadKey();
            #endregion
        }
    }
}