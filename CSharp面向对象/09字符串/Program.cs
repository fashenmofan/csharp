﻿namespace _09字符串
{
    class Program
    {
        public static void Main(string[] args)
        {
            //字符串的不可变性
            string name = "张三";
            name = "孙权";
            Console.WriteLine(name);
            Console.ReadKey();

            //两者指向的堆空间地址相同
            string name1 = "张三";
            string name2 = "张三";
            Console.ReadKey();

            //可以将string类型 看做是char类型的一个只读数组
            string s = "abcdefg";
            //s[0] = 'b';   不能这样做，因为是只读的
            //首先将字符串转换成char类型的数组
            char[] chs = s.ToCharArray();
            chs[0] = 'b';
            //将字符数组转换为我们的字符串
            s = new string(chs);
            //既然可以将string看作char类型的只读数组，所以我可以通过下标去访问字符串中的某个元素
            Console.WriteLine(s[0]);
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}