﻿namespace _04构造函数
{
    public class Program
    {
        static void Main(string[] args)
        {
            Student zsStudent = new Student("张三", 18, '男', 100, 100, 100);
            Student xlStudent = new Student("小兰", 18, '女', 90, 90, 90);

            Student czStudent = new Student(100, 100, 100);

            Student thisStudent = new Student("this", 100, 100, 100);
            Console.ReadKey(true);
        }
    }
}